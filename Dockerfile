FROM adoptopenjdk:11-jre-openj9

ADD target/demo-0.0.1-SNAPSHOT.jar /my.jar

CMD ["java", "-jar", "/my.jar"]