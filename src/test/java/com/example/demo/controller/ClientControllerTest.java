package com.example.demo.controller;

import com.example.demo.model.Client;
import com.example.demo.service.ClientServiceI;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//@RunWith(SpringRunner.class)
//@WebMvcTest(ClientController.class)

/**
 * for junit5
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest
class ClientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClientServiceI clientService;

    private Client fillClient(Long id, String name) {
        Client client = new Client();
        client.setId(id);
        client.setName(name);
        return client;
    }

    @Test
    public void testGetRequest() throws Exception {
        List<Client> clients = Arrays.asList(
                fillClient(1l, "client1"),
                fillClient(2L, "client2"));
        given(clientService.findAll()).willReturn(clients);
        mockMvc.perform(get("/client"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$.[?(@.id==1)].name", Matchers.iterableWithSize(1)))
                .andExpect(jsonPath("$.[?(@.id==1)].name", Matchers.containsInAnyOrder("client1")))
                .andExpect(jsonPath("$.[?(@.id==2)].name", Matchers.iterableWithSize(1)))
                .andExpect(jsonPath("$.[?(@.id==2)].name", Matchers.containsInAnyOrder("client2")));
    }

    @Test
    public void testDeleteSecondClient() throws Exception {
        Mockito.doNothing().when(clientService).deleteClientById(Mockito.anyLong());
        mockMvc.perform(delete("/client/2"))
                .andExpect(status().isNoContent())
                .andExpect(content().bytes(new byte[0]));
    }

    @Test
    public void testDeleteRequestException() throws Exception {
        Mockito.doThrow(new RuntimeException("Error")).when(clientService).deleteClientById(Mockito.anyLong());
        mockMvc.perform(delete("/client/3"))
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message", Matchers.is("Error")));
    }
}