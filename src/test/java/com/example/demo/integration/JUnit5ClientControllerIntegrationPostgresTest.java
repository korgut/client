package com.example.demo.integration;

import com.example.demo.dao.ClientRepository;
import com.example.demo.dao.entity.ClientEntity;
import lombok.Setter;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Testcontainers
@SpringBootTest
@AutoConfigureMockMvc(printOnlyOnFailure = false)
@ContextConfiguration(initializers = {JUnit5ClientControllerIntegrationPostgresTest.Initializer.class})
class JUnit5ClientControllerIntegrationPostgresTest {

    @Container
    private static final PostgreSQLContainer<?> sqlContainer = new PostgreSQLContainer<>("postgres:11");

    @Autowired
    private MockMvc mockMvc;

    @Setter(onMethod_ = {@Autowired})
    private ClientRepository repository;

    @AfterEach
    void cleanTableInPostgres() {
        repository.deleteAll();
    }


    private ClientEntity createClientEntity(String name) {
        ClientEntity client = new ClientEntity();
        client.setName(name);
        return client;
    }

    @Test
    void testGetRest_Success() throws Exception {
        ClientEntity c1 = repository.save(createClientEntity("c1"));
        System.out.println(c1.getCreatedDateTime());
        mockMvc.perform(get("/client"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].id", Matchers.notNullValue()))
                .andExpect(jsonPath("$[0].name", Matchers.is("c1")));
    }

    @Test
    void testGetRest_ErrorDBIsEmpty() throws Exception {
        mockMvc.perform(get("/client"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message", Matchers.is("DB client is empty!")));
    }

    @Test
    void testDeleteRest_Success() throws Exception {
        ClientEntity createdClient = repository.save(createClientEntity("c2"));
        mockMvc.perform(delete("/client/{id}", createdClient.getId()))
                .andExpect(status().isNoContent())
                .andDo(MockMvcResultHandlers.print());
        Assertions.assertEquals(0, repository.count(), "Ожидалось, что после удаления клиента должно остатьса 0 клиентов");
    }

    @Test
    void testPatchRest_Success() throws Exception {
        ClientEntity createdClient = repository.save(createClientEntity("c1"));
        Long id = createdClient.getId();
        mockMvc.perform(patch("/client")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":" + id + ",\"name\": \"newClient\"}"))
                .andExpect(status().isNoContent());

        Optional<ClientEntity> clientEntityOptional = repository.findById(id);
        Assertions.assertTrue(clientEntityOptional.isPresent(), "В базе должен быть клиент с таким id");
        ClientEntity clientUpdate = clientEntityOptional.get();
        Assertions.assertEquals("newClient", clientUpdate.getName(), "Ожидаем, что имя клиента в БД равно новому!");
        testAudit(clientUpdate.getCreatedDateTime(), clientUpdate.getLastModifiedDateTime());
    }

    @Test
    void testPutRest_Success() throws Exception {
        mockMvc.perform(put("/client")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\": \"c1\"}")).andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", Matchers.notNullValue()))
                .andExpect(jsonPath("$.name", Matchers.is("c1")));
        ClientEntity entity = repository.findAll().get(0);
        Assertions.assertNull(entity.getLastModifiedDateTime());
        Assertions.assertNotNull(entity.getCreatedDateTime());
    }

    private void testAudit(LocalDateTime createdDateTime, LocalDateTime lastModifiedDateTime) {
        Assertions.assertNotNull(createdDateTime);
        Assertions.assertNotNull(lastModifiedDateTime);
        Assertions.assertNotEquals(createdDateTime, lastModifiedDateTime, "Ожидаем, что дата создания и изменения разные!");

    }

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + sqlContainer.getJdbcUrl(),
                    "spring.datasource.username=" + sqlContainer.getUsername(),
                    "spring.datasource.password=" + sqlContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}