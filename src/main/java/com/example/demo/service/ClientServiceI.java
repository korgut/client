package com.example.demo.service;

import com.example.demo.model.Client;

import java.util.List;

public interface ClientServiceI {

    Client save(Client request);

    List<Client> findAll();

    void deleteClientById(Long id);

    void updateClientNameById(Client client);
}
