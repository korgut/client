package com.example.demo.service;

import com.example.demo.dao.ClientRepository;
import com.example.demo.dao.entity.ClientEntity;
import com.example.demo.dao.entity.QClientEntity;
import com.example.demo.exception.ClientNotFoundException;
import com.example.demo.model.Client;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.Setter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClientService implements ClientServiceI, InitializingBean {

    @Setter(onMethod_ = {@Autowired})
    private ClientRepository repository;

    @Setter(onMethod_ = {@Autowired})
    private EntityManager entityManager;

    private JPAQueryFactory jpaQuery;

    @Override
    public void afterPropertiesSet() throws Exception {
        jpaQuery = new JPAQueryFactory(entityManager);
    }

    private ClientEntity convertRequestToDaoEntity(Client client) {
        ClientEntity entity = new ClientEntity();
        entity.setId(client.getId());
        entity.setName(client.getName());
        return entity;
    }

    private Client convertDaoEntityToResponse(ClientEntity entity) {
        Client client = new Client();
        client.setId(entity.getId());
        client.setName(entity.getName());
        return client;
    }

    public Client save(Client request) {
        ClientEntity client = repository.save(convertRequestToDaoEntity(request));
        return convertDaoEntityToResponse(client);
    }

    public List<Client> findAll() {
        List<ClientEntity> clients = repository.findAll();
        if (clients.isEmpty()) {
            throw new ClientNotFoundException("DB client is empty!");
        }
        return clients.stream()
                .map(this::convertDaoEntityToResponse)
                .collect(Collectors.toList());
    }

    public void deleteClientById(Long id) {
        repository.deleteById(id);
    }


    @Transactional
    public void updateClientNameById(Client client) {
        Optional<ClientEntity> clientOptional = repository.findById(client.getId());
        clientOptional.ifPresent(clientEntity -> {
            clientEntity.setName(client.getName());
            repository.save(clientEntity);
        });
        //при тестировании Junit+docker и аудита JPA в данной конструкции updateDateTime не проставлялась
//        jpaQuery.update(QClientEntity.clientEntity)
//                .where(QClientEntity.clientEntity.id.eq(client.getId()))
//                .set(QClientEntity.clientEntity.name, client.getName())
//                .execute();
    }
}