package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Client {

    @JsonProperty("id")
    private Long id;

    @JsonProperty(value = "name")
    private String name;
}
