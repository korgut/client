package com.example.demo.dao;

import com.querydsl.core.types.dsl.EntityPathBase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface ExtensionRepository<T, ID extends Serializable, QT extends EntityPathBase<T>>
        extends JpaRepository<T, ID>,
        QuerydslPredicateExecutor<T>, QuerydslBinderCustomizer<QT> {
    @Override
    default void customize(QuerydslBindings bindings, QT root) {

    }
}
