package com.example.demo.dao;

import com.example.demo.dao.entity.ClientEntity;
import com.example.demo.dao.entity.QClientEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends ExtensionRepository<ClientEntity, Long, QClientEntity> {

}
