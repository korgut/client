package com.example.demo.controller;

import com.example.demo.model.Client;
import com.example.demo.service.ClientServiceI;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController {

    @Setter(onMethod_ = @Autowired)
    private ClientServiceI service;

    @ResponseStatus(HttpStatus.CREATED)
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public Client saveClient(@Validated @RequestBody Client request) {
        return service.save(request);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Client> getClients() {
        return service.findAll();
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id:\\d+}")
    public void deleteClientById(@PathVariable("id") Long id) {
        service.deleteClientById(id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateClientNameById(@RequestBody Client client) {
        service.updateClientNameById(client);
    }
}